---
title: Events
---

## Summary

Each operation on the memory store is an `Event`. The events are sent via a primitive `Channel`, which can be accessed from multiple threads.
The channel serves as a `fifo queue`, processing messages in the order they are received.

## Events

The `Events` are stored under a sealed class. There are two shared properties across all sub data classes.

* *key*: The name of the key location.
* *answer*: A callback that will notify once the message has been processed.

### Add

Create a new entry in the key value map.

  * *key*: The name of the key location.
  * *value*: The value to Add

  This adds a new entry to the `MemoryStore`. If a key is already present and `allowUpsert` is enabled, it will update the object in place. If
  `allowUpsert` is disabled, and the key is already in the map an error will be raised.

### Add With Expiration

Create a new entry in the key value map, that will expire some time in the future.

  * *key*: The name of the key location.
  * *value*: The value to Add
  * *expires*: A `java.util.Date` that will expire some time in the future.

### Delete

Remove an entry from the `MemoryStore` with the given key.

* *key*: The name of the key location.

### update

Update the value of an exsting key.

## Change Expiration

Change the time a key expires in the `MemoryStore`

  * *key*: The name of the key location.
  * *expires*: A `java.util.Date` that will expire some time in the future.

## Delete Expiration

Remove an expiration time for a given key.

* *key*: The name of the key location.
