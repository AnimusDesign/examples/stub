---
sidebar_position: 2
title: Summary
---
import Mermaid from '@theme/Mermaid';

## Summary

The Memory Store is an in memory key value store that can be accessed from multiple threads. Key design goals

* 95th Percentile response of 1ms
* 99th Percentile response of 5ms
* Handle up to 10 Million Entries
* Allow for expiration of objects
* Keys are of string
* Values are of any

## Consistency

The key store by default is eventually consistent. That is to say that upon adding an entry
it will be eventually consistent. The caller can also wait until the key has been added.

**Note: The default behavior is to be eventually consistent. You will need to explicilty block to ensure a key is inserted.**

## Concurrency

### Threading

The memory store will run in it's own thread pool, adjacent from the main thread. Below is a flow chart of the thread
hierarchy. These are based on coroutine [Dispatchers](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-dispatchers/index.html).
Where the main application launches on the `Defaullt/Main` thread, while the `MemoryStore` launches on `IO`.

<Mermaid chart={`
  graph TD
    subgraph Main
    M[App]
    end
    A[Start] --> M
    A[Start] --> S
    M[App]
    subgraph IO
    S[MemoryStore]
    S[MemoryStore]-->E[EventListener]
    S[MemoryStore]-->X[ExpirationProcessor]
    end
`}/>

### Adding Objects

While the map looks and operates like a normal `HashMap/MutableMap`. It exposes a [channel](https://kotlinlang.org/docs/channels.html)
that can be accessed by adjacent threads. The channel acts like an event bus listener, and using the noted `UNLiMITED`
treats it like a `FIFO` queue. 

*Upstream Note*
```
When capacity is Channel.UNLIMITED &mdash; it creates a channel with effectively unlimited buffer. This channel has a 
linked-list buffer of unlimited capacity (limited only by available memory). Sending to this channel never suspends, 
and trySend always succeeds.
```

As [events](./events) are sent to the queue, the channel retrieves them and processes them in a queue. Calling typical
`HashMap` functions will send a message to the queue. Once digested it will be performed on the core internal memory store. 
On creation of the map it can be shared with multiple threads, and it will `fan in` to the central `MemoryStore` channel.

```kotlin
fun main() = runBlocking {
    val store = MemoryStore() // Runs on a seperate thread
    store.add("TestKey", "TestValue") // Sends to the MemoryStore EventListener
    store.close()
}
```

<Mermaid chart={`
  graph TD
    subgraph Main
    M[App]
    end
    A[Start] --> M
    A[Start] --> S
    M[App]
    M-. MemoryStoreEvents.Add .->E
    subgraph IO
    S[MemoryStore]
    S[MemoryStore]-->E[EventListener]
    S[MemoryStore]-->X[ExpirationProcessor]
    end
`} />`

While this is eventually consistent you may wish to use this immediately. Amending the call with `await` will wait until
the queue has processed the messaged and completed successfully. If an error was encountered this `await` would also
raise the error.

```kotlin
fun main() = runBlocking {
    val store = MemoryStore() // Runs on a seperate thread
    store.add("TestKey", "TestValue").await() // Sends to the MemoryStore EventListener
    println(store["TestKey"])
    store.close()
}
```

### Expiration

Expiration of keys is supported via an event `AddWithExpiration`. This eventa takes a time when the given event will 
expire. On addition besides adding the key, it will also add the expiration to an adjacent map, containing keys and expiration.

**Note: There is only one expiration value. If you change it will be updated.**

This launches as a separate coroutine under the `MemoryStore` thread. It starts a continuous loop, that will find
any keys whose expiration is less than or equal to the current time, then delete them. *This does not directly 
manipulate the internal map. Instead, it fires a message to delete the object.*
