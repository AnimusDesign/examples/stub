---
title: Future
---

## Partitioning

The store by at this time is based on a single flat index, being the initial key. This can be portioned out
to a partitioned memory store. Where a key creates a nested map, acting as separate tables, and distributing the
memory impact.

## Back Pressure

The main channel has potential to become saturated, and take too long. The channel listener is a coroutine meaning
it will fire off a coroutine for each message. But distributing this via multiple channels may help reduce the load. 
In the case of a high throughput channel a thread context can be created for each event, and a channel for each event.
Or conversely the channels can be partitioned per intent, aggregating them by commonality. In case of the aforementioned
partitioning, the channels can also be partitioned based off of a key name space. This would create multiple smaller
channels rather than a single large channel.

## Cancelling an Event

A consumer may wish to cancel or, aggregate changes. Hypothetically if a user were to call `Add`, `Update`, and `Delete`.
When `Add` hadn't been even completed, that can be reduced.

## Event Log

While this at it's core is volatile. The event log can be distributed, and replayed to rebuild a memory store. Allowing
for recovery on a failure. 

## Multi Platform

The coroutine primitives are largely multi-platform. This can be expanded to also run via `NodeJS`, `Deno`, or `LLVM`.
It would be beneficial to vet each runtime for best performance.

## CI/CD Integration

At this time the runners are shared. It would be beneficial to get repeatable SLA checks with different 
vm/hardware classes. Measuring performance drift across instance types.

## Developer Experience

A number of the inbuilt operators can be extended allowing for accessing the `MemoryStore` as if it were a `Map`.

The async can also be improved i.e.

```kotlin
try {
    store.add("TestKey", "TestValue").await() //wordy
} catch (e:Exception) {
    logger.error(e) { "Failed to add in item"}
}

!(store["TestKey"] = "TestValue") // Override not operator with await
store["TestKey"] !== "TestValue" // Override not equal operator
add "TestKey" of "TestValue" to store block // Use of infix functions and DSLs
// The following feels the most compatible with the core language

store.addNow {
    "TestKeyOne" to "TestValueOne",
    "TestKeyTwo" to "TestValueTwo"
} // Feels more like a natural language extensions, and easier discoverability.
```

## Metrics 

Add metrics to the MemoryStore Class.
