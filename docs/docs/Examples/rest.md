---
title: REST
---

### Summary

* **Location**: *examples/server*
* **Run**: `./gradlew :examples:server:run`

## Explanation

This is a simple [Ktor](https://ktor.io/) server that allows creating and retrieval of keys. Starting the server is blocking,
so any curl queries will need to be made in an adjacent terminal.

## Sample Queries

```bash
~➜  walaroo git:(master) ✗ curl localhost:8080/store/TestKey
{"key":"TestKey","value":"null"}%
➜  walaroo git:(master) ✗  curl -X POST    -H 'Content-Type: application/json'   -d '{"key": "TestKey", "value": "TestValue"}' localhost:8080/store
{"success":true}%                                                                                                                                                                                                   ➜  walaroo git:(master) ✗
➜  walaroo git:(master) ✗ curl localhost:8080/store/TestKey
{"key":"TestKey","value":"TestValue"}%
```
