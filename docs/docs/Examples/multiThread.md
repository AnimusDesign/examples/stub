--- 
title: Multi Threaded
---

### Summary 

* **Location**: *examples/multithreads*
* **Run**: `./gradlew :examples:multithreads:run`

## Explanation

This examples spawns up four single thread executors. Then creates 10 key values in the memory store concurrently. Waiting
for all to complete, then printing out the store.

