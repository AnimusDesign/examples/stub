---
tile: SLA
---

## Summary

* **Location**: `./benchmarks/sla`
* **Running**:  `./gradlew :benchmarks:sla:test`

## Explanation

The SLA is a unit test that ensures operations meet the anticipated performance levels. At this time they are as follows.

* Retrieve a key with a 95th percentile of 1ms and a 99th percentile of 5ms.

The test spawns several stores, and creates a set of keys to simulate multiple size sets.

* *1,000*
* *10,000*
* *100,000*
* *1,000,000*

*While there is an additional test to test up to ten million keys, it did not work overly well with JUNIT.*

Each Test is curried via an internal helper method. Adding new tests just change the parameters to execute a new
test set.

* First the keys are added to the store, this is blocking ensuring all keys are settled.
  * The keys match the index number, and the value is a randomly genered uuid.
* Latencies are calculated from a sample size, at this time the sample size is half of ther over all keys i.e. 1,000 keys is 500 samples.
  * Each sample randomly picks a key, trying to ensure a variation in resultant latency and not hitting a warm entry.
  * These are mapped into a list
* The list is then passed into a function that ensures it matches the anticipated percentile response time.
