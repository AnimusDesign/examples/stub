---
title: Summary
---

## Dev Environment

Required tools

* [Open JDK 11](https://adoptopenjdk.net/)
* *Optional* [GraalVM 11 Native Image](https://www.graalvm.org/downloads/)

### Docker Development

A docker image is provided in the root file. That will run a projector instance to quickly spin up a development
environment with all the necessary libraries. It supports both Projector and VS Code remote dev.

*Warning: The image is rather large around 8GB*

*Warning: This is based off Intellij Ultimate a license is required. But evaluate may be used.*

```bash
docker build -f DevDocker .
```


At the end you will be presented with an  image sha i.e. `Successfully built 4a70391e51f7`.

```bash
~/ > docker run -it --rm -v $(pwd):/src -p 8887:8887 -p 8822:6622  -d 4a70391e51f7
4e180f62a3786a81c06dbd8944203afa68af464fcee27fec32b693e3ecf05f33
```
### Projector

You can now access the Intellij ide in a browser via `http://localhost:8887`.

*Warning without SSL some keybindings copy/paste/etc. will be intercepted and dropped by the browser.*

### Shell

You can also execute basic commands like gradle via the shell.

```bash
~/ > docker exec -it 4e180f62a3786a81c06dbd8944203afa68af464fcee27fec32b693e3ecf05f33 bash
...
[developer@4e180f62a378 src]$ ./gradlew clean ktlintFormat build
```

### Visual Studio Code

[Visual Studio code can be accessed via the remote ssh plugin.](https://code.visualstudio.com/docs/remote/ssh)

### SSH

SSH is enabled in the container, but password authentication is not enabled. You will need to first add your public
key to the `developer` user.

```bash
~/ > docker exec -it d3ae9d4afb67c9dec7e56c7bb9c79504af4b02341996993407a0048b3cf26d86 bash
[developer@d3ae9d4afb67 ~]$ mkdir .ssh
[developer@d3ae9d4afb67 ~]$ vim .ssh/authorized_keys
[developer@d3ae9d4afb67 ~]$ exit
...
ssh developer@localhost -p 8822
```

## Graal vs. Jar

In light of testing that is to be done in the future. The project was configured with intended output of
Graal and a JAR. No benchmarking has been done yet. 
