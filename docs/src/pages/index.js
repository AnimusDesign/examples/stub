import React from 'react';
import Layout from '@theme/Layout';


export default function Home() {
  return (
    <Layout
      title={`Sample Memory Store Docs`}
      description="Description will go into a meta tag in <head />">
      <main>
        <h1>
          <a href="/examples/stub/docs/intro">Docs</a>
        </h1>
        <h2>Summary</h2>
        <p>
          A simple in memory store sample project.
        </p>
      </main>
    </Layout>
  );
}
