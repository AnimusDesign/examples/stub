import java.net.URI

rootProject.name = "walaroo"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("org.jetbrains.dokka")) {
                // Update Version Build Source if being changed.
                useVersion("1.6.0")
            }
            if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization")) {
                // Update Version Build Source if being changed.
                useVersion("1.6.0")
            }
            if (
                requested.id.id.startsWith("org.gradle.kotlin.kotlin-dsl")
            ) {
                useVersion("2.1.7")
            }
        }
    }
    repositories {
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }
        maven { url = java.net.URI("https://kotlin.bintray.com/kotlin-dev") }
    }
}

include(":lib:memorystore:")

include(":examples:multithreads:")
include(":examples:server:")

include(":benchmarks:graal:")
include(":benchmarks:jar:")
include(":benchmarks:sla:")
