package com.stub.examples.multithreads

import com.stub.lib.memorystore.MemoryStore
import kotlinx.coroutines.*
import mu.KotlinLogging
import java.util.concurrent.Executors

fun main() = runBlocking {
  val logger = KotlinLogging.logger {}
  val store = MemoryStore()

  /**
   * Create a range of 4  allowing for multiple threads
   */
  (0..4).map { thread ->
    // Create a new thread context
    val localThreadScope = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
    // Collect the responses as a deferred
    val responses = withContext(localThreadScope) {
      (0..10).map { index ->
        logger.info { "Adding $index from context:${this.coroutineContext} $this" }
        store.add("Thread${thread}Index$index", index)
      }
    }
    // This thread can now be closed as we have retrieved all the responses from sending
    localThreadScope.close()
    responses
  }
    .flatten() // We get a nested List<List<Deferred>>
    .awaitAll() // Await as the store defaults to eventual consistency but you can wait
  // We wait all items have been flushed to the store.
  /**
   * Stop the store background processes
   *
   */
  store.stop()

  println(store)
  println("Hello")
}
