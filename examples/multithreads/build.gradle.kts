import design.animus.kotlin.contract.Versions.Dependencies

plugins {
  application
}

kotlin {
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main"))
      dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
        implementation(project(":lib:memorystore:"))
      }
    }
    test {
      kotlin.setSrcDirs(mutableListOf("test"))
      resources.setSrcDirs(listOf("test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

application {
  mainClassName = "com.walaroo.examples.multithreads.MainKt"
}
