package com.stub.examples.server.plugins

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.metrics.micrometer.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.micrometer.prometheus.*
import org.slf4j.event.*

fun Application.configureMonitoring() {
  install(CallLogging) {
    level = Level.INFO
    filter { call -> call.request.path().startsWith("/") }
  }

  val appMicrometerRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

  install(MicrometerMetrics) {
    registry = appMicrometerRegistry
    // ...
  }

  routing {
    get("/metrics-micrometer") {
      call.respond(appMicrometerRegistry.scrape())
    }
  }
}
