package com.stub.examples.server

import com.stub.lib.memorystore.MemoryStore
import com.stub.examples.server.plugins.configureHTTP
import com.stub.examples.server.plugins.configureMonitoring
import com.stub.examples.server.plugins.configureRouting
import com.stub.examples.server.plugins.configureSerialization
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

internal lateinit var GlobalStore: MemoryStore

fun main() {
  GlobalStore = MemoryStore()
  embeddedServer(Netty, port = 8080, host = "0.0.0.0") {
    configureRouting()
    configureHTTP()
    configureMonitoring()
    configureSerialization()
    routing {
      storeRoutes()
    }
  }.start(wait = true)
}
