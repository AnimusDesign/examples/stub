package com.stub.examples.server.records

import com.stub.lib.memorystore.records.MemoryStoreKey
import kotlinx.serialization.Serializable

@Serializable
data class CreateStoreObject(
  val key: MemoryStoreKey,
  val value: String,
  val timeToLive: Long = 0
)

@Serializable
data class StoreSuccess(
  val success: Boolean
)

@Serializable
data class StoreObject(
  val key: MemoryStoreKey,
  val value: String
)
