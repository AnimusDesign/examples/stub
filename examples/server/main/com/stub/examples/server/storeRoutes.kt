package com.stub.examples.server

import com.stub.lib.memorystore.MemoryStore
import com.stub.examples.server.records.CreateStoreObject
import com.stub.examples.server.records.StoreObject
import com.stub.examples.server.records.StoreSuccess
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import java.time.Instant
import java.util.*

fun Route.storeRoutes() {
  val store = MemoryStore()
  route("/store") {
    post {
      val createStoreObject = call.receive<CreateStoreObject>()
      val rsp = when (createStoreObject.timeToLive) {
        0L -> {
          GlobalStore.add(createStoreObject.key, createStoreObject.value)
          store.add(createStoreObject.key, createStoreObject.value)
        }
        else -> {
          GlobalStore.addWithExpiration(
            createStoreObject.key,
            createStoreObject.value,
            Date.from(Instant.now().plusSeconds(createStoreObject.timeToLive))
          )
          store.addWithExpiration(
            createStoreObject.key,
            createStoreObject.value,
            Date.from(Instant.now().plusSeconds(createStoreObject.timeToLive))
          )
        }
      }
      rsp.await()
      println(store)
      call.respond(
        StoreSuccess(true)
      )
    }
    get() {
      call.respond(store)
    }
    get("/{key}") {
      if (!call.parameters.contains("key")) {
        throw Exception("Key is required in url to get a key i.e. /store/MyKey")
      }
      val key = call.parameters["key"]!!
      val data = store[key]
      println(store)
      call.respond(
        StoreObject(key, data.toString())
      )
    }
  }
}
