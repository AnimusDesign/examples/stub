import design.animus.kotlin.contract.Versions.Dependencies

plugins {
  application
}

kotlin {
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main"))
      dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
        implementation(project(":lib:memorystore:"))
        implementation("io.ktor:ktor-server-core:${Dependencies.ktor}")
        implementation("io.ktor:ktor-server-host-common:${Dependencies.ktor}")
        implementation("io.ktor:ktor-metrics:${Dependencies.ktor}")
        implementation("io.ktor:ktor-metrics-micrometer:${Dependencies.ktor}")
        implementation("io.micrometer:micrometer-registry-prometheus:${Dependencies.prometheus}")
        implementation("io.ktor:ktor-serialization:${Dependencies.ktor}")
        implementation("io.ktor:ktor-server-netty:${Dependencies.ktor}")
      }
    }
    test {
      kotlin.setSrcDirs(mutableListOf("test"))
      resources.setSrcDirs(listOf("test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

application {
  mainClassName = "com.wallaroo.examples.server.MainKt"
}
