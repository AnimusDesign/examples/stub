package com.stub.lib.memorystore.records

import kotlinx.coroutines.CompletableDeferred
import java.util.*

sealed class MemoryStoreEvents(
  open val key: MemoryStoreKey,
) {
  val answer = CompletableDeferred<Unit>()

  data class Add(override val key: MemoryStoreKey, val value: Any) : MemoryStoreEvents(key)

  data class AddWithExpiration(override val key: MemoryStoreKey, val expires: Date, val value: Any) :
    MemoryStoreEvents(key)

  data class Delete(override val key: MemoryStoreKey) : MemoryStoreEvents(key)
  data class Update(override val key: MemoryStoreKey, val value: Any) : MemoryStoreEvents(key)
  data class ChangeExpiration(override val key: MemoryStoreKey, val expires: Date) : MemoryStoreEvents(key)
  data class DeleteExpiration(override val key: MemoryStoreKey) : MemoryStoreEvents(key)
}
