package com.stub.lib.memorystore.records

import java.util.*

data class MemoryStoreLogEvent(
  val time: Date,
  val event: MemoryStoreEvents
)
