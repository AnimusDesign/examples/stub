package com.stub.lib.memorystore

import com.stub.lib.memorystore.records.MemoryStoreEvents
import com.stub.lib.memorystore.records.MemoryStoreKey
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.coroutines.CoroutineContext

/**
 * @param name Name of the memory store.
 * @param allowUpsert Allow `Add` to change an existing key.
 */
class MemoryStore(
  private val name: String = "Default",
  private val allowUpsert: Boolean = true,
) {
  private val memoryStore: ConcurrentHashMap<MemoryStoreKey, Any> = ConcurrentHashMap()
  private val memoryStoreEventList: Channel<MemoryStoreEvents> = Channel(UNLIMITED)
  private val memoryStoreExpiration: ConcurrentHashMap<MemoryStoreKey, Date> = ConcurrentHashMap()
  private lateinit var context: CoroutineContext
  private lateinit var process: Job
  private val scope: CoroutineScope = CoroutineScope(
    Dispatchers.IO
  )

  init {
    process = scope.launch {
      context = this.coroutineContext
      logger.info { "Starting memory store: $name" }
      launch {
        logger.debug { "Starting Memory Event Processor" }
        memoryStoreEventList.receiveAsFlow().collect { event ->
          try {
            resolver(event)
            event.answer.complete(Unit)
          } catch (e: Exception) {
            logger.error { "Encountered error processing event: $event" }
            event.answer.completeExceptionally(e)
          }
        }
      }
      launch {
        logger.debug("Launching Expiration Watcher")
        while (true) {
          val now = Instant.now()
          memoryStoreExpiration.filter { (_, date) ->
            date.toInstant() <= now
          }.forEach { (key, _) ->
            try {
              delete(key).await()
              memoryStoreExpiration.remove(key)
            } catch (e: Exception) {
              logger.warn { "Failed to delete key: $key in the anticipated window leaving for next loop" }
            }
          }
          delay(2)
        }
      }
    }
  }

  suspend fun stop() {
    memoryStoreEventList.close()
    process.cancel()
    scope.cancel("Requested to stop.")
  }

  private suspend fun resolver(event: MemoryStoreEvents) {
    when (event) {
      is MemoryStoreEvents.Add -> {
        if (memoryStore.containsKey(event.key) && !allowUpsert) {
          val message =
            "Tried adding key: ${event.key} to store but already present, but upsert is not allowed."
          logger.error { message }
          throw IllegalArgumentException(message)
        }
        logger.debug { "Adding key: ${event.key} with value:${event.value} to store" }
        memoryStore[event.key] = event.value
      }
      is MemoryStoreEvents.Delete -> {
        logger.debug { "Received event to delete key: ${event.key}" }
        memoryStore.remove(event.key)
      }
      is MemoryStoreEvents.Update -> {
        logger.debug { "Received event to update key: ${event.key}" }
        memoryStore[event.key] = event.value
      }
      is MemoryStoreEvents.AddWithExpiration -> {
        logger.debug { "Adding entry with an expiration.  " }
        memoryStore[event.key] = event.value
        memoryStoreExpiration[event.key] = event.expires
      }
      is MemoryStoreEvents.ChangeExpiration -> {
        logger.debug { "Received event to change expiration of key: ${event.key}" }
        memoryStoreExpiration[event.key] = event.expires
      }
      is MemoryStoreEvents.DeleteExpiration -> {
        logger.debug { "Received event to remove expiration on key: ${event.key}" }
        memoryStoreExpiration.remove(event.key)
      }
    }
  }

  private suspend fun sendMessage(message: MemoryStoreEvents): CompletableDeferred<Unit> {
    memoryStoreEventList.send(message)
    return message.answer
  }

  suspend fun add(key: MemoryStoreKey, value: Any): CompletableDeferred<Unit> =
    sendMessage(MemoryStoreEvents.Add(key = key, value = value))

  suspend fun addWithExpiration(key: MemoryStoreKey, value: Any, expires: Date) = sendMessage(
    MemoryStoreEvents.AddWithExpiration(key, expires, value)
  )

  suspend fun changeExpiration(key: MemoryStoreKey, expires: Date) = sendMessage(
    MemoryStoreEvents.ChangeExpiration(key, expires)
  )

  suspend fun delete(key: MemoryStoreKey) = sendMessage(
    MemoryStoreEvents.Delete(key)
  )

  suspend fun deleteExpiration(key: MemoryStoreKey) = sendMessage(
    MemoryStoreEvents.DeleteExpiration(key)
  )

  operator fun get(key: String): Any? = this.memoryStore.getOrDefault(key, null)

  suspend fun hasKey(key: MemoryStoreKey): Boolean = memoryStore.containsKey(key)

  override fun toString(): String {
    return memoryStore.toString()
  }

  suspend fun update(key: MemoryStoreKey, value: Any): CompletableDeferred<Unit> =
    sendMessage(MemoryStoreEvents.Update(key = key, value = value))
}
