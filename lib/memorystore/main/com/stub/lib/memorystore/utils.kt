package com.stub.lib.memorystore

import mu.KotlinLogging

internal val logger = KotlinLogging.logger("com.walaroo.example.lib.memorystore")
