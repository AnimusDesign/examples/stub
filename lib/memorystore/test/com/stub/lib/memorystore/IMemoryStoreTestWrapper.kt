package com.stub.lib.memorystore

import com.stub.lib.memorystore.records.MemoryStoreKey
import kotlinx.coroutines.runBlocking

interface IMemoryStoreTestWrapper {
  fun testWrapper(
    key: MemoryStoreKey,
    value: Any,
    block: suspend (MemoryStore, MemoryStoreKey, Any) -> Unit
  ) = runBlocking {
    val store = MemoryStore()
    block(store, key, value)
    store.stop()
  }
}
