package com.stub.lib.memorystore

import kotlinx.coroutines.delay
import java.time.Instant
import java.util.*
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class AddWithExpiration : IMemoryStoreTestWrapper {

  @Test
  fun testBasicExpiration() = testWrapper("testBasicExpiration", true) { store, key, value ->
    val now = Instant.now()
    val additionalSeconds = 2L
    val future = now.plusSeconds(additionalSeconds)
    store.addWithExpiration(key, value, Date.from(future)).await()
    while (Instant.now() < future) {
      assertTrue { store[key] == value }
    }
    delay(500)
    assertFalse { store.hasKey(key) }
  }

  @Test
  fun testUpdatedExpiration() = testWrapper("testBasicExpiration", true) { store, key, value ->
    val now = Instant.now()
    val additionalSeconds = 2L
    val future = now.plusSeconds(additionalSeconds)
    store.addWithExpiration(key, value, Date.from(future)).await()

    val newNow = Instant.now()
    val newFuture = newNow.plusSeconds(additionalSeconds)
    store.changeExpiration(key, Date.from(newFuture))
    while (Instant.now() < newFuture) {
      assertTrue { store[key] == value }
    }
    delay(500)
    assertFalse { store.hasKey(key) }
  }

  @Test
  fun testExpirationWithAnUpdatedValue() = testWrapper("testBasicExpiration", true) { store, key, value ->
    val now = Instant.now()
    val additionalSeconds = 2L
    val future = now.plusSeconds(additionalSeconds)
    store.addWithExpiration(key, value, Date.from(future)).await()
    val newValue = "NewValue"
    store.add(key, newValue).await()
    while (Instant.now() < future) {
      assertTrue { store[key] == newValue }
    }
    delay(500)
    assertFalse { store.hasKey(key) }
  }

  @Test
  fun testDeleteExpiration() = testWrapper("testDeleteExpiration", true) { store, key, value ->
    val now = Instant.now()
    val additionalSeconds = 2L
    val future = now.plusSeconds(additionalSeconds)
    store.addWithExpiration(key, value, Date.from(future)).await()
    store.deleteExpiration(key).await()
    delay(additionalSeconds * 1000)
    assertTrue { store.hasKey(key) }
    assertTrue { store[key] == value }
  }
}
