package com.stub.lib.memorystore

import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class TestMemoryStoreEvents : IMemoryStoreTestWrapper {

  @Test
  fun testAdd() = testWrapper("testAdd", true) { store, key, value ->
    store.add(key, value).await()
    assertTrue {
      store[key] == value
    }
  }

  @Test
  fun testUpdate() = testWrapper("testUpdate", true) { store, key, value ->
    store.add(key, value).await()
    assertTrue { store[key] == value }
    val newValue = "UpdatedValue$value"
    store.update(key, newValue).await()
    assertTrue { store[key] == newValue }
  }

  @Test
  fun testDelete() = testWrapper("testUpdate", true) { store, key, value ->
    store.add(key, value).await()
    assertTrue { store[key] == value }
    store.delete(key).await()
    assertFalse { store.hasKey(key) }
  }

  @Test
  fun testUpsertSuccessWhenAllowed() = testWrapper("testUpdate", true) { store, key, value ->
    store.add(key, value).await()
    assertTrue { store[key] == value }
    val newValue = "UpdatedValue$value"
    store.add(key, newValue).await()
    assertTrue { store[key] == newValue }
  }

  @Test
  fun testUpsertFailWhenNotAllowed() = runBlocking {
    val store = MemoryStore(allowUpsert = false)
    val key = "testDisallowUpsert"
    val value = true
    assertFails {
      store.add(key, value).await()
      store.add(key, "NewValue").await()
    }
    Unit
  }
}
