package design.animus.kotlin.contract

object Versions {
    object Plugins {
        const val kotlin = Dependencies.kotlin
    }

    object Dependencies {
        const val kotlin = "1.6.0"
        const val serialization = "1.3.1"
        const val coroutine = "1.5.2"
        const val junit = "4.12"
        const val kotlinLogging = "2.0.11"
        const val logback = "1.2.7"
        const val MPDataTypes = "0.1.3"
        const val ktor = "1.6.5"
        const val prometheus = "1.6.3"
    }
}
