buildscript {
    repositories {
        mavenLocal()
        maven { url = uri("https://plugins.gradle.org/m2/") }
        maven { url = uri("https://animusdesign-repository.appspot.com") }
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx") }
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.0")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:10.2.0")
        classpath("org.jetbrains.kotlin:kotlin-serialization:1.6.0")
    }
}

allprojects {
    repositories {
        mavenLocal()
        mavenCentral()
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://oss.jfrog.org/oss-release-local") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx/") }
    }
}

subprojects {
    if (File("${this.projectDir}/build.gradle.kts").isFile) {
        apply(plugin = "org.jetbrains.kotlin.jvm")
        apply(plugin = "org.jetbrains.kotlin.plugin.serialization")
        apply(plugin = "maven-publish")
        apply(plugin = "org.jlleitschuh.gradle.ktlint")
        configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
            debug.set(true)
            verbose.set(true)
            android.set(false)
            enableExperimentalRules.set(true)
            additionalEditorconfigFile.set(file("$rootDir/.editorconfig"))
            filter {
                include("**/main/**", "**/src/main/**", "**/test/**", "**/src/test/**")
                exclude("**/generated/**", "generated")
            }
        }
        val version: String by project
        val baseGroup = "com.walaroo.example"
        group = baseGroup
        this.version = version
        val compileKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
        compileKotlin.kotlinOptions {
            jvmTarget = "11"
        }
        val compileTestKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
        compileTestKotlin.kotlinOptions {
            jvmTarget = "11"
        }
    }
}
