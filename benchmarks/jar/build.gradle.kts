import design.animus.kotlin.contract.Versions.Dependencies

plugins {
  id("org.graalvm.buildtools.native") version "0.9.4"
  id("com.github.johnrengelman.shadow") version "7.0.0"
}
kotlin {
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main"))
      dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
      }
    }
    test {
      kotlin.setSrcDirs(mutableListOf("test"))
      resources.setSrcDirs(listOf("test/resources"))
      dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

nativeBuild {
  // Main options
  imageName.set("memoryStore") // The name of the native image, defaults to the project name
  mainClass.set("com.walaroo.example.lib.memorystore.MemoryStoreKt") // The main class to use, defaults to the application.mainClass
  debug.set(true) // Determines if debug info should be generated, defaults to false
  verbose.set(true) // Add verbose output, defaults to false
  fallback.set(true) // Sets the fallback mode of native-image, defaults to false
  buildArgs.add("-H:ReflectionConfigurationFiles=${projectDir.absolutePath}/graal.json")
  sharedLibrary.set(false) // Determines if image is a shared library, defaults to false if `java-library` plugin isn't included
  agent.set(true) // Enables the reflection agent. Can be also set on command line using '-Pagent'
  useFatJar.set(true) // Instead of passing each jar individually, builds a fat jar
}
