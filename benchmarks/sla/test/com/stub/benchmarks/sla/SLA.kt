package com.stub.benchmarks.sla

import com.stub.lib.memorystore.MemoryStore
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import kotlin.math.ceil
import kotlin.system.measureNanoTime
import kotlin.test.Test
import kotlin.test.assertTrue

class SLA {
  private fun percentile(latencies: List<Long>, percentile: Double): Long {
    val index = ceil(percentile / 100.0 * latencies.size).toInt()
    return latencies[index - 1]
  }

  private fun getRandomKey(min: Int, max: Int) = ThreadLocalRandom.current().nextInt(min, max + 1)

  private fun slaTestWrapper(sampleSize: Int, maxKeys: Int, ninetyFifthPercentile: Int, ninetyNinthPercentile: Int) =
    runBlocking {
      val store = MemoryStore()
      (0..maxKeys).map { key ->
        store.add("$key", UUID.randomUUID())
      }.awaitAll()
      val latencies = (0..sampleSize).map {
        val testKey = getRandomKey(0, maxKeys)
        measureNanoTime {
          val data = store["$testKey"]
        }
      }
      assertTrue { percentile(latencies, 95.0) <= (ninetyFifthPercentile * 1000000) }
      assertTrue { percentile(latencies, 99.0) <= (ninetyNinthPercentile * 1000000) }
      store.stop()
      Unit
    }

  @Test
  fun test1000Keys() = slaTestWrapper(500, 1000, 1, 5)

  @Test
  fun test10000Keys() = slaTestWrapper(5000, 10000, 1, 5)

  @Test
  fun test100000Keys() = slaTestWrapper(50000, 100000, 1, 5)

  @Test
  fun test1000000() = slaTestWrapper(500000, 1000000, 1, 5)
}
